package garden.bots.builders

fun json(init: JsonObjectBuilder.() -> Unit) = JsonObjectBuilder().apply(init).json
