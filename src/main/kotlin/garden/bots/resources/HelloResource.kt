package garden.bots.resources

import garden.bots.builders.json
import io.vertx.core.json.JsonObject
import javax.ws.rs.Produces
import javax.ws.rs.GET
import javax.ws.rs.POST
import javax.ws.rs.Path


@Path("/hello")
class HelloResource {
  /**
   * Test it:
   * curl -X GET http://localhost:8080/hello
   */
  @Produces("application/json; charset=utf-8") @GET
  fun hello(): JsonObject {
    return json {
      "message" to "Hello World!"
    }
  }

  /**
   * Test it:
   * curl -H "Content-Type: application/json" -X POST -d '{"who":"Bob Morane"}' http://localhost:8080/hello
   */
  @Produces("application/json; charset=utf-8") @POST
  fun hello(data: JsonObject): JsonObject {
    return json {
      "message" to "hello ${data.getString("who")}"
    }
  }

}
